/*******************************************************************/
/*                                                                 */
/*                      ADOBE CONFIDENTIAL                         */
/*                   _ _ _ _ _ _ _ _ _ _ _ _ _                     */
/*                                                                 */
/* Copyright 2007 Adobe Systems Incorporated                       */
/* All Rights Reserved.                                            */
/*                                                                 */
/* NOTICE:  All information contained herein is, and remains the   */
/* property of Adobe Systems Incorporated and its suppliers, if    */
/* any.  The intellectual and technical concepts contained         */
/* herein are proprietary to Adobe Systems Incorporated and its    */
/* suppliers and may be covered by U.S. and Foreign Patents,       */
/* patents in process, and are protected by trade secret or        */
/* copyright law.  Dissemination of this information or            */
/* reproduction of this material is strictly forbidden unless      */
/* prior written permission is obtained from Adobe Systems         */
/* Incorporated.                                                   */
/*                                                                 */
/*******************************************************************/

#include "Marker_Mover.h"

static AEGP_PluginID	S_my_id				= 0;
static SPBasicSuite		*sP 				= NULL;
static AEGP_Command		S_Marker_Mover_cmd = 0;

			
static A_Err
CommandHook(
	AEGP_GlobalRefcon	plugin_refconPV,		/* >> */
	AEGP_CommandRefcon	refconPV,				/* >> */
	AEGP_Command		command,				/* >> */
	AEGP_HookPriority	hook_priority,			/* >> */
	A_Boolean			already_handledB,		/* >> */
	A_Boolean			*handledPB)				/* << */
{
    
	A_Err 				err 					= A_Err_NONE, 
						err2 					= A_Err_NONE;
	AEGP_StreamRefH		layerMarker_streamH		= NULL,
						compMarker_streamH		= NULL;
    A_Time				layerTimeT				= {0,0},
                        compTimeT               = {0,0};
    AEGP_KeyframeIndex  compKeyIndexL           = 0;
	AEGP_LayerH			layerH 					= NULL;
    AEGP_CompH          compH                   = NULL;
    A_long              numMarkers              = 0;
    AEGP_StreamValue2	val;
	
	AEGP_SuiteHandler	suites(sP);

	if (command == S_Marker_Mover_cmd) {
		try {
            
			ERR(suites.LayerSuite8()->AEGP_GetActiveLayer(&layerH));
            ERR(suites.StreamSuite4()->AEGP_GetNewLayerStream(S_my_id, layerH, AEGP_LayerStream_MARKER, &layerMarker_streamH));
            
            ERR(suites.LayerSuite8()->AEGP_GetLayerParentComp(layerH, &compH));
            ERR(suites.CompSuite10()->AEGP_GetNewCompMarkerStream(S_my_id, compH, &compMarker_streamH));
            
            ERR(suites.KeyframeSuite4()->AEGP_GetStreamNumKFs(layerMarker_streamH, &numMarkers));
            
            if (!err && numMarkers > 0){
                
                ERR(suites.UtilitySuite3()->AEGP_StartUndoGroup(STR(StrID_Name)));
                
                for (AEGP_KeyframeIndex iL = 0; !err && (iL < numMarkers); ++iL) {
                    
                    AEFX_CLR_STRUCT(val); // wash between uses
                    
                    ERR2(suites.KeyframeSuite4()->AEGP_GetNewKeyframeValue(S_my_id, layerMarker_streamH, iL, &val));
                    ERR2(suites.KeyframeSuite4()->AEGP_GetKeyframeTime(layerMarker_streamH, iL, AEGP_LTimeMode_LayerTime, &layerTimeT));
                    ERR2(suites.LayerSuite8()->AEGP_ConvertLayerToCompTime(layerH, &layerTimeT, &compTimeT));
                    ERR2(suites.KeyframeSuite4()->AEGP_InsertKeyframe(compMarker_streamH, AEGP_LTimeMode_CompTime, &compTimeT, &compKeyIndexL));
                    ERR2(suites.KeyframeSuite4()->AEGP_SetKeyframeValue(compMarker_streamH, compKeyIndexL, &val));
                    
                }
                ERR2(suites.StreamSuite4()->AEGP_DisposeStreamValue(&val));
                ERR2(suites.UtilitySuite3()->AEGP_EndUndoGroup());
            }
            if (layerMarker_streamH){ERR2(suites.StreamSuite4()->AEGP_DisposeStream(layerMarker_streamH));}
            if (compMarker_streamH){ERR2(suites.StreamSuite4()->AEGP_DisposeStream(compMarker_streamH));}
		} catch (A_Err &thrown_err){
			err = thrown_err;
		}
	}
	return err;		
}

static A_Err
UpdateMenuHook(
			   AEGP_GlobalRefcon		plugin_refconPV,	/* >> */
			   AEGP_UpdateMenuRefcon	refconPV,			/* >> */
			   AEGP_WindowType			active_window)		/* >> */
			   {
	A_Err 				err 			=	A_Err_NONE;
	AEGP_SuiteHandler	suites(sP);
	AEGP_LayerH			layerH 			=	NULL;
	
	try {
		ERR(suites.LayerSuite8()->AEGP_GetActiveLayer(&layerH));
		if (layerH){
            ERR(suites.CommandSuite1()->AEGP_SetMenuCommandName(S_Marker_Mover_cmd, STR(StrID_Name)));
            ERR(suites.CommandSuite1()->AEGP_EnableCommand(S_Marker_Mover_cmd));
		} else {
			ERR(suites.CommandSuite1()->AEGP_SetMenuCommandName(S_Marker_Mover_cmd, STR(StrID_Selection)));
            ERR(suites.CommandSuite1()->AEGP_DisableCommand(S_Marker_Mover_cmd));
		}
	} catch (A_Err &thrown_err){
		err = thrown_err;
	}
	return err;
}

A_Err
EntryPointFunc(
	struct SPBasicSuite		*pica_basicP,		/* >> */
	A_long				 	major_versionL,		/* >> */		
	A_long					minor_versionL,		/* >> */		
	AEGP_PluginID			aegp_plugin_id,		/* >> */
	AEGP_GlobalRefcon		*global_refconP)	/* << */
{
	A_Err 			err					=	A_Err_NONE, 
					err2 				=	A_Err_NONE;
					sP					=	pica_basicP;
					S_my_id				=	aegp_plugin_id;	

	try {
		AEGP_SuiteHandler	suites(sP);

		ERR(suites.CommandSuite1()->AEGP_GetUniqueCommand(&S_Marker_Mover_cmd));

		ERR(suites.CommandSuite1()->AEGP_InsertMenuCommand(	S_Marker_Mover_cmd, 
															STR(StrID_Selection), 
															AEGP_Menu_LAYER, 
															AEGP_MENU_INSERT_SORTED));

		ERR(suites.RegisterSuite5()->AEGP_RegisterCommandHook(	S_my_id, 
																AEGP_HP_BeforeAE, 
																AEGP_Command_ALL, 
																CommandHook, 
																NULL));

		ERR(suites.RegisterSuite5()->AEGP_RegisterUpdateMenuHook(S_my_id, UpdateMenuHook, NULL));

		if (err) { // not !err, err!
			ERR2(suites.UtilitySuite3()->AEGP_ReportInfo(S_my_id, STR(StrID_Troubles)));
		}
	} catch (A_Err &thrown_err){
		err = thrown_err;
	}
	return err;
}
