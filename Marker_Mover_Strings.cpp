// Marker_Mover_Strings.cpp

#include "Marker_Mover.h"

typedef struct {
	unsigned long	index;
	char			str[256];
} TableString;

TableString		g_strs[StrID_NUMTYPES] = {
	StrID_NONE,						"",
	StrID_Name,						"Move Markers to Comp",
	StrID_Selection,				"Move Markers to Comp",
	StrID_CommandName,				"Move Markers",
	StrID_Troubles,					"Marker_Mover: Problems encountered during marker move"
};

char	*GetStringPtr(int strNum)
{
	return g_strs[strNum].str;
}
	
